function onClick() {
    let cost = document.querySelector("#cost");
    let quantity = document.querySelector("#quantity");
    if(!Number.isNaN(parseInt(cost.value)) && !Number.isNaN(parseInt(quantity.value))){
        let result = document.querySelector(".result");
        result.innerHTML = ("Общая стоимость : "
        + (parseInt(cost.value) * parseInt(quantity.value)) + " Рублей ! " );
    }
    else
        alert("Введены некоректные символы. Заполните форму заново!");
    
}

window.addEventListener("DOMContentLoaded", function (event) {
    let btn = document.querySelector(".myBtn");
    btn.addEventListener("click", onClick);
});
